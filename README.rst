========
Overview
========

CHAI MSP Data

* Free software: GNU Lesser General Public License v3 or later (LGPLv3+)

Installation
============

::

    pip install chaimsp

You can also install the in-development version with::

    pip install https://gitlab.com/chai-digital-tools-lac/chaimsp/-/archive/main/chaimsp-main.zip


Documentation
=============


To use the project:

.. code-block:: python

    import chaimsp
    chaimsp.longest()


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
