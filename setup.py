#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import io
import re
from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import splitext

from setuptools import find_packages
from setuptools import setup


def read(*names, **kwargs):
    with io.open(join(dirname(__file__), *names), encoding=kwargs.get('encoding', 'utf8')) as fh:
        return fh.read()


setup(
    name='chaimsp',
    version='0.1.0',
    license='LGPL-3.0-or-later',
    description='CHAI MSP Data',
    long_description='{}\n{}'.format(
        re.compile('^.. start-badges.*^.. end-badges', re.M | re.S).sub('', read('README.rst')),
        re.sub(':[a-z]+:`~?(.*?)`', r'``\1``', read('CHANGELOG.rst')),
    ),
    author='Dr. Esteban Munoz',
    author_email='emunoz.ic@clintonhealthaccess.org',
    url='https://gitlab.com/chai-digital-tools-lac/data-processing-msp',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    include_package_data=True,
    zip_safe=False,
    # classifiers=[
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        # 'Development Status :: 5 - Production/Stable',
        # 'Intended Audience :: Developers',
        # 'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)'
        # 'Operating System :: Unix',
        # 'Operating System :: POSIX',
        # 'Operating System :: Microsoft :: Windows',
        # 'Programming Language :: Python',
        # 'Programming Language :: Python :: 3',
        # 'Programming Language :: Python :: 3 :: Only',
        # 'Programming Language :: Python :: 3.7',
        # 'Programming Language :: Python :: 3.8',
        # 'Programming Language :: Python :: 3.9',
        # 'Programming Language :: Python :: 3.10',
        # 'Programming Language :: Python :: 3.11',
        # 'Programming Language :: Python :: Implementation :: CPython',
        # 'Programming Language :: Python :: Implementation :: PyPy',
        # uncomment if you test on these interpreters:
        # 'Programming Language :: Python :: Implementation :: IronPython',
        # 'Programming Language :: Python :: Implementation :: Jython',
        # 'Programming Language :: Python :: Implementation :: Stackless',
        # 'Topic :: Utilities',
        # 'Private :: Do Not Upload',
    # ],
    project_urls={
        'Changelog': 'https://gitlab.com/chai-digital-tools-lac/data-processing-msp/blob/master/CHANGELOG.rst',
        'Issue Tracker': 'https://gitlab.com/chai-digital-tools-lac/data-processing-msp/issues',
    },
    keywords=[
        'MSP Ecuador', 
    ],
    python_requires='>=3.7',
    install_requires=[
        'boto3==1.26.16',
        'pandas==1.3.2',
        'geopandas==0.10.2',
        'SQLAlchemy==1.4.24',
        'redis==4.4.0',
        'pickle5==0.0.12',
        'psycopg2-binary==2.9.5',
        'GeoAlchemy2==0.12.5',
    ],
    # extras_require={
        # eg:
        #   'rst': ['docutils>=0.11'],
        #   ':python_version=="2.6"': ['argparse'],
    # },
    # entry_points={
    #     'console_scripts': [
    #         'chaimsp = chaimsp.cli:main',
    #     ]
    # },
)
