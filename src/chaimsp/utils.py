import os
import datetime
from datetime import date, timedelta
import json
from pprint import pprint
import io

import boto3
import pandas as pd
import geopandas as gpd
from sqlalchemy import create_engine
import pickle5 as pickle
import redis

from io import StringIO

class MSP_ETL(object):
    session = boto3.session.Session()
    def __init__(
        self, 
        redis_host='localhost', redis_port=6379, redis_db=0,
        s3_access_key_id='123', s3_secret_access_key='123', 
        s3_endpoint='https://s3.chai-digital-tools-lac.com', s3_cubo='test',
        key_done='test_done', key_geo='test_geo', key_db='test',
        db_engine='postgresql', db_user='postgres', db_password='postgres', 
        db_host='localhost', db_port='5432', db_table='postgres'):
        self.redis = redis.Redis(host=redis_host, port=redis_port, db=redis_db)
        self.s3 = self.session.client(
            service_name='s3',
            aws_access_key_id=s3_access_key_id,
            aws_secret_access_key=s3_secret_access_key,
            endpoint_url=s3_endpoint,
        )
        self.s3_paginator = self.s3.get_paginator('list_objects_v2')
        self.s3_cubo = s3_cubo
        self.data = False
        self._dop_cols = [
            'zona', 'region', 'provincia', 'canton',
            'poblacion', 'provincia_poblacion', 'canton_poblacion', 
            'lat', 'lng']
        self.key_done = key_done
        self.key_geo = key_geo
        self.key_db = key_db
        self.engine = create_engine('{}://{}:{}@{}:{}/{}'.format(db_engine, db_user, db_password, db_host, db_port, db_table))
        self._get_geo()
        self._get_existing()

    def _fetch_data(self, delta_days=100):
        query_str = """
        select * from "{}" where fecha >= current_date at time zone 'UTC' - interval '{} days'
        """.format(self.key_db, delta_days)
        df = gpd.GeoDataFrame(pd.read_sql_query(query_str, con=self.engine))
        df['geometry'] = gpd.GeoSeries.from_wkb(df['geometry'])
        return df

    def upload_data(self, check_duplicates=True, delta_days=100):
        
        if check_duplicates:
            historic_data = self._fetch_data(delta_days=delta_days)
            upload_data = self.data
            # print(historic_data)
            # print(historic_data.dtypes)
            # print(upload_data)
            for c_dt in upload_data.columns:
                h_dt = historic_data.loc[:, c_dt].dtype
                upload_data.loc[:, c_dt] = upload_data.loc[:, c_dt].astype(h_dt)
            
            upload_data_st = upload_data.loc[:, [c for c in upload_data.columns if c not in ['lat', 'lng']]]
            historic_data_st = historic_data.loc[:, [c for c in historic_data.columns if c not in ['lat', 'lng']]]
            duplicate_data_inx = upload_data_st.isin(historic_data_st).all(axis=1)
            #[]
            # print(upload_data_st.isin(historic_data_st))
            # print(upload_data_st)
            # print(historic_data_st)
            # print(upload_data.dtypes)
            # print(duplicate_data_inx)
            # print(self.data)
            self.data = self.data.loc[~duplicate_data_inx]
            for c_dt in self.data.columns:
                h_dt = historic_data.loc[:, c_dt].dtype
                if h_dt == 'float64' and c_dt not in ['lat', 'lng']:
                    try:
                        self.data.loc[:, c_dt] = self.data.loc[:, c_dt].astype('int64')
                    except: 
                        self.data.loc[:, c_dt] = self.data.loc[:, c_dt].astype(h_dt)
            # print(self.data)

        if self.data.shape[0] >= 1:
            # try:
            if isinstance(self.data, gpd.GeoDataFrame):
                # print('GeoDataFrame')
                # print(self.data)
                self.data.to_postgis(self.key_db, self.engine, if_exists='append')
            else:
                print('DataFrame')
                self.data.to_sql(self.key_db, self.engine, if_exists='append')
            # except:
                # print("can't upload data")
        else:
            print('duplicate data or empty data-set')

    def _delete_cols(self, out_data, skip_col):
        drop_df_cols = [c for c in self._dop_cols if c in  out_data.columns and c != skip_col]
        out_data = out_data.drop(drop_df_cols, axis=1)
        return out_data

    def _fix_canton(self, out_data, skip_col):
        if 'provincia' in out_data.columns:
            out_data.loc[((out_data.canton == 'Olmedo') & (out_data.provincia == 'Manabí')), 'canton'] = "Olmedo (Manabí)"
            out_data.loc[((out_data.canton == 'Olmedo') & (out_data.provincia == 'Loja')), 'canton'] = "Olmedo (Loja)"
        out_data = self._delete_cols(out_data, skip_col)
        return out_data

    def _fix_geo(self, out_data):
        #self._get_geo()
        col = out_data.columns
        skip_col = False
        
        if 'canton' in col:
            skip_col = 'canton'
            out_data = self._fix_canton(out_data, skip_col)
            this_geo = self.geo
        elif 'provincia' in col:
            skip_col = 'provincia'
            out_data = self._delete_cols(out_data, skip_col)
            this_geo = self.geo.loc[self.geo.cabezera_provincia]
            this_geo = this_geo.drop(['canton', 'canton_poblacion'], axis=1)
        elif 'region' in col:
            skip_col = 'region'
            this_geo = self.geo.loc[self.geo.cabezera_region]
        elif 'zona' in col:
            skip_col = 'zona'
            this_geo = self.geo.loc[self.geo.cabezera_zona]
        
        if not skip_col:
            print('Nivel geográfico no definido, columnas: {}'.format(';'.join(col)))
            return out_data
        else:
            print('Nivel geográfico: {}'.format(skip_col))
            cols = ['cabezera_provincia','cabezera_region','cabezera_zona', 'geometry']
            for col in cols:
                try:
                    out_data = out_data.drop(col, axis=1)
                except:
                    out_data = out_data

            cols = ['cabezera_provincia','cabezera_region','cabezera_zona']
            for col in cols:
                try:
                    this_geo = this_geo.drop(col, axis=1)
                except:
                    this_geo = this_geo

            out_data = out_data.merge(this_geo, left_on=skip_col, right_on=skip_col, how='left')
            out_data = gpd.GeoDataFrame(out_data)
            return out_data

    def _get_geo(self):
        try:
            self.geo = pickle.loads(self.redis.get(self.key_geo))
        except:
            print("TODO: fix pickle version (py3.6 to py3.7)")
            print("nice pickle! i save a csv on s3 as well")
            self.get_data(prefix='tmp/cantones2.csv', is_dataframe=True)
            self.geo = self.data
            
    def reset_done(self):
        self.redis.set(self.key_done, ','.encode('utf-8'))
        self._get_existing()

    def _get_existing(self):
        try:
            _done = self.redis.get(self.key_done).decode('utf-8')
            _done = _done.split(',')
        except:
            _done = list()
            self.redis.set(self.key_done, _done.encode('utf-8'))
        self.done = _done

    def _get_data(self, key_data, is_dataframe=False):
        response = self.s3.get_object(Bucket=self.s3_cubo, Key=key_data)
        
        status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
        
        if status == 200:
            print(f"Successful S3 get_object response. Status - {status} -- {key_data}")
            if is_dataframe:
                print('200_get data as DataFrame')
                with io.StringIO() as csv_buffer:
                    print('streaming from S3')
                    df_file = [[i.replace('\n', '') for i in row.decode('utf-8').split(',')] for row in response.get("Body").readlines()]
                    df = pd.DataFrame(df_file)
                    df.columns = df.iloc[0,:]
                    df = df.iloc[1:,:]
                    df['geometry'] = gpd.GeoSeries.from_wkt(df['geometry'])
                    df = gpd.GeoDataFrame(df)
                    df = df.set_crs('epsg:4326')
                    if 'tmp/cantones2.csv' in key_data:
                        self.geo = df
            else:
                print('200_get data as JSON')
                print("'json load from b'DataFrame'")
                df = pd.DataFrame(json.loads(response.get("Body").readlines()[0])['data'])
            return df
        else:
            print(f"Unsuccessful S3 get_object response. Status - {status}")
            return False

    def get_data(self, delta_days=100, prefix='COVID-CAMAS/2022/', is_dataframe=False, geo=False):
        
        if isinstance(geo, gpd.GeoDataFrame):
            print('geo')
            self.geo = geo
        else:
            print('no geo')
        if is_dataframe: print('get data as DataFrame')
        self._get_existing()
        today = date.today() - timedelta(days=delta_days)
        filtro = "Contents[?to_string(LastModified)>='\"{} 00:00:00-00:00\"'].Key".format(today.strftime("%Y-%m-%d"))

        s3_iterator = self.s3_paginator.paginate(Bucket=self.s3_cubo, Prefix=prefix)

        filtered_iterator = s3_iterator.search(filtro)
        
        all_data = list()
        for key_data in filtered_iterator:
            print('\treading {}'.format(key_data))
            if key_data not in self.done:
                camas_df = self._get_data(key_data, is_dataframe=is_dataframe)
                self.done.append(key_data)
                if isinstance(camas_df, pd.DataFrame):
                    all_data.append(camas_df)
        if len(all_data) >= 1:
            data_concat = pd.concat(all_data)
        else:
            data_concat = False

        if isinstance(data_concat, pd.DataFrame):
            data_concat.columns = [c.lower() for c in data_concat.columns]
            try:
                data_out = self._fix_geo(data_concat)
            except:
                data_out = data_concat
                print("can't fix geo, OK if is geo")
            
            try:
                data_out = fix_date(data_out)
            except:
                print("can't fix data, should work on geo")

            self.data = data_out
            self.redis.set(self.key_done, ','.join(self.done).encode('utf-8'))
        else:
            self.data = False


def fix_date(out_data:pd.DataFrame, col_names=['fecha', 'created_at', 'date'], date_col=False, date_format=False):
    """Format data values.

    Args:
      out_data:
        An pandas.DataFrame instance.
      col_names:
        A list of strings representing the posible column name containing
        a date vlue.
      date_col:
        specific colum name containing the data value.
      date_format:
        specific date format to use.

    Returns:
      A pandas.dataframe with formated date values.

    Raises:
      IOError: An error occurred accessing the smalltable.

    >>> fix_date(pd.DataFrame({'fecha': ['4-12-2022']}))
           fecha
    0 2022-12-04
    >>> fix_date(pd.DataFrame({'fecha': ['4-12-2022']}))
           fecha
    0 2022-12-04
    >>> fix_date(pd.DataFrame({'fecha': ['4/12/2022']}))
           fecha
    0 2022-12-04
    >>> fix_date(pd.DataFrame({'fecha': ['4-12-2022']}))
           fecha
    0 2022-12-04
    >>> fix_date(pd.DataFrame({'created_at': ['4-12-2022']}))
           fecha
    0 2022-12-04
    >>> fix_date(pd.DataFrame({'date': ['4-12-2022']}))
           fecha
    0 2022-12-04
    >>> fix_date(pd.DataFrame({'date': ['2022-12-4']}))
           fecha
    0 2022-12-04
    >>> fix_date(pd.DataFrame({'updated_at': ['4-12-2022']}), date_col='updated_at')
           fecha
    0 2022-12-04
    >>> fix_date(pd.DataFrame({'updated_at': ['4-12-22']}), date_col='updated_at', date_format='%d-%m-%y')
           fecha
    0 2022-12-04
    """
    fix_date = False
    if not date_col:
        for col_name in col_names:
            if col_name in out_data.columns:
                date_col = col_name
                fix_date = True
    else:
        if date_col in out_data.columns:
            fix_date = True

    if fix_date and not date_format:
        sample_date = out_data[date_col].iloc(0)[0]
        if '-' in sample_date:
            if len(sample_date.split('-')[0]) == 4:
                date_format = '%Y-%m-%d'
            else:
                date_format = '%d-%m-%Y'
        elif '/' in sample_date:
            date_format = '%d/%m/%Y'
        else:
            fix_date = False

    if fix_date:
        out_data[date_col] =  pd.to_datetime(out_data[date_col], format=date_format)
        old_cols = out_data.columns
        new_cols = [c.replace(date_col, 'fecha') for c in old_cols]
        out_data.columns = new_cols
    return out_data


def test():
    out_data = fix_date(pd.DataFrame({'fecha': ['4/12/2022']}))
    print(out_data)
    out_data = fix_date(pd.DataFrame({'fecha': ['4-12-2022']}))
    print(out_data)
    out_data = fix_date(pd.DataFrame({'created_at': ['4-12-2022']}))
    print(out_data)
    out_data = fix_date(pd.DataFrame({'date': ['4-12-2022']}))
    print(out_data)
    out_data = fix_date(pd.DataFrame({'date': ['2022-12-4']}))
    print(out_data)
    out_data = fix_date(pd.DataFrame({'updated_at': ['4-12-2022']}), date_col='updated_at')
    print(out_data)
    out_data = fix_date(pd.DataFrame({'updated_at': ['4-12-22']}), date_col='updated_at', date_format='%d-%m-%y')
    print(out_data)
    # settings = settings()
    # print(settings.redis)
    # print(settings.s3)
    # print('data-processing-msp')

def main():
    my_etl = MSP_ETL(
        redis_host='app.chai-digital-tools-lac.com', 
        redis_port=6380, 
        redis_db=0,
        s3_access_key_id='123', 
        s3_secret_access_key='123', 
        s3_endpoint='https://s3.chai-digital-tools-lac.com', 
        s3_cubo='test',
        key_done='camas_done', 
        key_geo='canton_geo', 
        key_db='camas',
        db_engine='postgresql', 
        db_user='chai', 
        db_password='chai', 
        db_host='15.235.116.50', 
        db_port='5433', 
        db_table='chai_bi'
    )
    my_etl.reset_done()
    # my_etl._get_geo()
    my_etl.get_data(delta_days=100, prefix='COVID-01CAMAS/2022/', geo=my_etl.geo)
    my_etl.upload_data(check_duplicates=True, delta_days=100)

if __name__ == "__main__":
    # import doctest
    # doctest.testmod()
    
    main()
